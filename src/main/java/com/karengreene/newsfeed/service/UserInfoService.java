package com.karengreene.newsfeed.service;

import com.karengreene.newsfeed.model.UserInfo;

public interface UserInfoService {

	public UserInfo create(UserInfo userInfo);
}
