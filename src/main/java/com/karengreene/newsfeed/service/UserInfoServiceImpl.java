package com.karengreene.newsfeed.service;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;

import com.karengreene.newsfeed.dao.UserInfoDAO;
import com.karengreene.newsfeed.model.UserInfo;

@Named
public class UserInfoServiceImpl implements UserInfoService {

	@Inject
	private UserInfoDAO userInfoDAO;
	
	
	public UserInfo create(UserInfo userInfo)
	{
		userInfo.setDateAdded(new Date());
		return userInfoDAO.insert(userInfo);
	}
	
	
}
