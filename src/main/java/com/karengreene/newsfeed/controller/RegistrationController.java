package com.karengreene.newsfeed.controller;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.karengreene.newsfeed.model.UserInfo;
import com.karengreene.newsfeed.service.UserInfoService;

@Controller
public class RegistrationController {

	@Inject
	public UserInfoService userInfoService;

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView showRegister(HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mav = new ModelAndView("register");
		mav.addObject("userInfo", new UserInfo());
		return mav;

	}

	@RequestMapping(value = "/registerProcess", method = RequestMethod.POST)
	public ModelAndView addUser(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("userInfo") UserInfo userInfo) {

		userInfoService.create(userInfo);

		return new ModelAndView("welcome", "firstName", userInfo.getFirstName());

	}
}
