package com.karengreene.newsfeed.dao;

import com.karengreene.newsfeed.model.UserInfo;

public interface UserInfoDAO {

	public UserInfo insert(UserInfo userInfo);
	
	public int update(UserInfo userInfo);
	
	public UserInfo getUserInfo(int Id);
	
}
