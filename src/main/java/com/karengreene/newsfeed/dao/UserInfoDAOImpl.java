package com.karengreene.newsfeed.dao;


import javax.inject.Inject;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.karengreene.newsfeed.model.UserInfo;

@Transactional
@Repository
public class UserInfoDAOImpl implements UserInfoDAO{

	
	@Inject
    private NamedParameterJdbcTemplate jdbcTemplate;
	private static final String TABLE_NAME = "USER_INFO";
	private static final String SQL = 
			" (first_name, last_name, email_address, password, date_added) values (:firstName, :lastName, :emailAddress, :password, :dateAdded)";
	
	public UserInfo insert(UserInfo userInfo) {
		
		KeyHolder holder = new GeneratedKeyHolder();
		
		StringBuilder sb = new StringBuilder();
		sb.append("insert into ");
		sb.append(TABLE_NAME);
		sb.append(SQL);
		
		
		SqlParameterSource parameters = new MapSqlParameterSource()
				.addValue("firstName", userInfo.getFirstName())
				.addValue("lastName", userInfo.getLastName())
				.addValue("emailAddress", userInfo.getEmailAddress())
				.addValue("password", userInfo.getPassword())
				.addValue("dateAdded", userInfo.getDateAdded());
		jdbcTemplate.update(sb.toString(), parameters, holder);
		userInfo.setId(holder.getKey().intValue());
		
		
		return userInfo;
	}

	public int update(UserInfo userInfo) {
		// TODO Auto-generated method stub
		return 0;
	}

	public UserInfo getUserInfo(int Id) {
		// TODO Auto-generated method stub
		return null;
	}

}
