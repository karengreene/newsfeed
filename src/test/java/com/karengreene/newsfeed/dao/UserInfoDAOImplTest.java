package com.karengreene.newsfeed.dao;

import static org.junit.Assert.assertNotNull;

import java.util.Date;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.karengreene.newsfeed.model.UserInfo;

import org.springframework.test.context.ContextConfiguration;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/testContext.xml")
@Transactional
public class UserInfoDAOImplTest { 
	
	@Inject 
	private UserInfoDAO userInfoDAO;
	
	@Test
	public void testCreateUserInfo() {
		
		UserInfo userInfo = new UserInfo();
		userInfo.setFirstName("Karen");
		userInfo.setLastName("Greene");
		userInfo.setEmailAddress("dummyemail@address.com");
		userInfo.setPassword("somepassword");
		userInfo.setDateAdded(new Date());
		
		userInfoDAO.insert(userInfo);
		assertNotNull(userInfo.getId());
		
	}
}
